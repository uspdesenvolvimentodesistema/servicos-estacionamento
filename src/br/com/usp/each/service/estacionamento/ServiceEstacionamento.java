package br.com.usp.each.service.estacionamento;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.usp.each.estacioaki.domain.Aluguel;
import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacioaki.domain.Vaga;
import br.com.usp.each.estacionaki.estacionamento.TarefasEstacionamento;

import com.google.gson.Gson;
@Path("/estacionamento")
public class ServiceEstacionamento {

	
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTrackInJSON() {
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(new TarefasEstacionamento().getEstacionamentos())).build(); 

	}
	private TarefasEstacionamento task = new TarefasEstacionamento(); 
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Login(Estacionamento estacionamento){
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.login(estacionamento))).build(); 
	}
	
	@POST
	@Path("/ocuparVaga")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response OcuparVaga(Vaga vaga){
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.OcuparVaga(vaga))).build(); 
	}
	
	@POST
	@Path("/liberarVaga")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response LiberarVaga(Vaga vaga){
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.LiberarVaga(vaga))).build(); 
	}
	
	@POST
	@Path("/find")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response find(Estacionamento e){
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.find(e))).build(); 
	}


	@POST
	@Path("/aluguelVaga")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response aluguelVaga(Vaga vaga){
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.aluguelVaga(vaga))).build(); 
	}
	
	@POST
	@Path("/reservaOcupada")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response reservaOcupada(Aluguel aluguel){
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.reservaOcupada(aluguel))).build(); 
	}

	@POST
	@Path("/atualizar")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response atualizar(Estacionamento estacionamento){
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.atualiza(estacionamento))).build(); 
	}
	
	@POST
	@Path("/cadastro")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response cadastro(Estacionamento estacionamento){
		Gson gson = new Gson();
		return Response.status(200).entity(gson.toJson(task.cadastrar(estacionamento))).build(); 
	}
	
}
